#include "Vector.h"
#include <iostream>
/*
The function initilaizes the vector array
input; the starting size of a vector
output; creates an array with the given size
*/
Vector::Vector(int n)
{
	if (n < 2) // if the size is lower then 2
		n = 2; // we address it like its 2
	this->_elements = new int[n]; // create an array with the given size
	this->_resizeFactor = n; // set the resizing size to be as the array size each time
	this->_capacity = n;
	this->_size = 0;
}
/*
D'tor that releases dynamic memory
input; this pointer
output; deletes the array that was created dynamicly.
*/
Vector::~Vector()
{
	delete[] this->_elements;
}
/*
The function returns size of vector
input; this pointer
output; returns size of vector
*/
int Vector::size() const
{
	return this->_size;
}

/*
The function returns capacity of vector
input; this pointer
output; returns the capacity of vector
*/
int Vector::capacity() const
{
	return this->_capacity;
}

/*
The function return vector's resizeFactor
input; this pointer
output; return vector's resizeFactor
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}

/*
The function return if the vector array is empty
input; this pointer
output; if the array vector is empty
*/
bool Vector::empty() const
{
	return this->_size == 0;
}
/*
The function adds element at the end of the vector
input; this pointer and a new value
output; adds to the array the new value
*/
void Vector::push_back(const int& val)
{
	int current_capacity = this->_capacity; // save the current capacity
	if (this->_size == current_capacity) // if the vector is full
	{
		int* oldArr = this->_elements; // we save the old array
		this->_elements = new int[current_capacity + this->_resizeFactor]; // we create new array with bigger size
		for (int i = 0; i < this->_size; i++) // and put in him all the old array value
			this->_elements[i] = oldArr[i]; // no need to delete the old pointer because it gets deleted at the end
		this->_capacity = current_capacity + this->_resizeFactor; // and set the capacity
		delete[] oldArr;
	}
	this->_elements[this->_size] = val; // in any case we add the value
	this->_size++; // and inc the size

}

/*
The function removes and returns the last element of the vector
input; this pointer
output; returns an int value of the last element in the vector
*/
int Vector::pop_back()
{
	if (this->empty())
	{
		std::cout << "error: pop from empty vector" << std::endl;
		return -9999;
	}
	else
	{
		this->_size--;
		return this->_elements[this->_size];
	}
}
/*
The function changes the capacity
input; new capcity and a this pointer
output; creates a new array in case
*/
void Vector::reserve(int n)
{
	if (this->_capacity < n) // the size must be atleast n
	{
		int grown = this->_capacity + this->_resizeFactor; // get the multi
		while (grown < n) // until it gets more then n
			grown += this->_resizeFactor; // add
		
		int* oldArr = this->_elements; // save the old array in the function scope
		this->_elements = new int[grown]; // create and deploy the new arr with the new size
		this->_capacity = grown; // set the new capacity

		for (int i = 0; i < this->_size; i++) // run until size
			this->_elements[i] = oldArr[i]; // and save values to the new array
		delete[] oldArr; // delete the old array, function scopre makes the pointer point to null in the end
	}
}

/*
the function changes _size to n, unless n is greater than the vector's capacity
input; new size and a this pointer
output; sets a new size to the vector
*/
void Vector::resize(int n)
{
	this->reserve(n); // it doesnt matter if n is smaller or bigger we sent it to the function where it gets checked
	this->_size = n; // and we se the new size
}

/*
The function assigns val to all elemnts
input; this pointer and a new value
output; assigns val to all elemnts
*/
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++) // set new values to be as val
		this->_elements[i] = val;
}

/*
same as above, if new elements added their value is val
input; this pointer , n value and a value
output; sets new size for the array and sets for the new values to be as value
*/
void Vector::resize(int n, const int& val)
{
	int old_size = this->_size; // set old value
	this->resize(n); // get new memory locations
	for (int i = old_size; this->_size; i++) // then run on all the new array allocations
		this->_elements[i] = val; // and set the value
}

/*
We need a copy constractor
input; gets a copy of a vector class
output; create a vector by copy
*/
Vector::Vector(const Vector& other)
{
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < other._size; i++)
		this->_elements[i] = other._elements[i];
}
/*
The function makes = with the copy contractor
input; something to = with 
output; create a vector
*/
Vector& Vector::operator=(const Vector& other)
{
	return *(new Vector(other));
}

/*
the function returns the n'th element
input; wanted index
output; the value in that index
*/
int& Vector::operator[](int n) const
{
	if (n > this->_size)
	{
		std::cout << "OverFlow - index error" << std::endl;
		return this->_elements[0];
	}
	else
		return this->_elements[n];
}
